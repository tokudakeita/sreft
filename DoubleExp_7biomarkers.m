//
//  DoubleExp_7biomarkers.m
//  NappModels
//
//  Created by Takaaki Ishida on 2015/09/21.
//  Copyright (c) 2015 Takaaki Ishida. All rights reserved.
//

#import "Napp.h"

#define NumBM   7

@interface DoubleExp_7biomarkers : NSObject {
    id delegate;
    real *prms, offsetT, offsetD, triggerA, tc[NumBM];
}

@end

enum {a1, b1, c1, a2, b2, c2, a3, b3, c3, a4, b4, c4, a5, b5, c5, a6, b6, c6, a7, b7, c7,
    plotMin, plotMax, enableW, w1, w2, w3, w4, w5, w6, w7,
    Age, Gender, ApoE,
    // y_gender
    y1_f, y2_f, y3_f, y4_f, y5_f, y6_f, y7_f,
    // y_apoe
    y1_23, y2_23, y3_23, y4_23, y5_23, y6_23, y7_23,
    y1_34, y2_34, y3_34, y4_34, y5_34, y6_34, y7_34,
    y1_44, y2_44, y3_44, y4_44, y5_44, y6_44, y7_44,
    // y_gender_apoe
    y1_f34, y2_f34, y3_f34, y4_f34, y5_f34, y6_f34, y7_f34,
    y1_f44, y2_f44, y3_f44, y4_f44, y5_f44, y6_f44, y7_f44,
    // y_age
    y1_age, y2_age, y3_age, y4_age, y5_age, y6_age, y7_age,
    // y_age_gender
    y1_ageF, y2_ageF, y3_ageF, y4_ageF, y5_ageF, y6_ageF, y7_ageF,
    // y_age_apoe
    y1_age34, y2_age34, y3_age34, y4_age34, y5_age34, y6_age34, y7_age34,
    y1_age44, y2_age44, y3_age44, y4_age44, y5_age44, y6_age44, y7_age44,
    // y_age_gender_apoe
    y1_ageF34, y2_ageF34, y3_ageF34, y4_ageF34, y5_ageF34, y6_ageF34, y7_ageF34,
    y1_ageF44, y2_ageF44, y3_ageF44, y4_ageF44, y5_ageF44, y6_ageF44, y7_ageF44,
    t_f, t_23, t_34, t_44, t_f34, t_f44,
    t_age, t_ageF, t_age34, t_age44, t_ageF34, t_ageF44,
    MeanX1, MeanX2, MeanX3, MeanX4, MeanX5, MeanX6, MeanX7,
    MeanY1, MeanY2, MeanY3, MeanY4, MeanY5, MeanY6, MeanY7,
    Count1, Count2, Count3, Count4, Count5, Count6, Count7};

#define NONE -100
#define w_      prms[w1 + i]
#define MeanX_  prms[MeanX1 + i]
#define MeanY_  prms[MeanY1 + i]
#define Count_  prms[Count1 + i]
#define y_f         prms[y1_f + i]
#define y_23        prms[y1_23 + i]
#define y_34        prms[y1_34 + i]
#define y_44        prms[y1_44 + i]
#define y_f34       prms[y1_f34 + i]
#define y_f44       prms[y1_f44 + i]
#define y_age       prms[y1_age + i]
#define y_ageF      prms[y1_ageF + i]
#define y_age34     prms[y1_age34 + i]
#define y_age44     prms[y1_age44 + i]
#define y_ageF34    prms[y1_ageF34 + i]
#define y_ageF44    prms[y1_ageF44 + i]
#define NumPrms     (Count7 + 1)

#define GeneralMeanAge  70
#define GeneralTriggerAge   60

@implementation DoubleExp_7biomarkers

#include "NappModelRoutine.h"

- (int)functionType {
    return 0;       // analytical equation
}

- (int)numberOfParameters {
    return NumPrms;
}

- (int)numberOfEvaluates {
    return NumBM;
}

- (NSString*)parameterNameStringAt:(int)index {
    switch(index) {
        case a1:            return @"a1";
        case b1:            return @"b1";
        case c1:            return @"c1";
        case a2:            return @"a2";
        case b2:            return @"b2";
        case c2:            return @"c2";
        case a3:            return @"a3";
        case b3:            return @"b3";
        case c3:            return @"c3";
        case a4:            return @"a4";
        case b4:            return @"b4";
        case c4:            return @"c4";
        case a5:            return @"a5";
        case b5:            return @"b5";
        case c5:            return @"c5";
        case a6:            return @"a6";
        case b6:            return @"b6";
        case c6:            return @"c6";
        case a7:            return @"a7";
        case b7:            return @"b7";
        case c7:            return @"c7";
        case plotMin:       return @"plotMin";
        case plotMax:       return @"plotMax";
        case enableW:       return @"enableW";
        case w1:            return @"w1";
        case w2:            return @"w2";
        case w3:            return @"w3";
        case w4:            return @"w4";
        case w5:            return @"w5";
        case w6:            return @"w6";
        case w7:            return @"w7";
        case Age:           return @"Age";
        case Gender:        return @"Gender";
        case ApoE:          return @"ApoE";
        case y1_f:          return @"y1_f";
        case y2_f:          return @"y2_f";
        case y3_f:          return @"y3_f";
        case y4_f:          return @"y4_f";
        case y5_f:          return @"y5_f";
        case y6_f:          return @"y6_f";
        case y7_f:          return @"y7_f";
        case y1_23:         return @"y1_23";
        case y2_23:         return @"y2_23";
        case y3_23:         return @"y3_23";
        case y4_23:         return @"y4_23";
        case y5_23:         return @"y5_23";
        case y6_23:         return @"y6_23";
        case y7_23:         return @"y7_23";
        case y1_34:         return @"y1_34";
        case y2_34:         return @"y2_34";
        case y3_34:         return @"y3_34";
        case y4_34:         return @"y4_34";
        case y5_34:         return @"y5_34";
        case y6_34:         return @"y6_34";
        case y7_34:         return @"y7_34";
        case y1_44:         return @"y1_44";
        case y2_44:         return @"y2_44";
        case y3_44:         return @"y3_44";
        case y4_44:         return @"y4_44";
        case y5_44:         return @"y5_44";
        case y6_44:         return @"y6_44";
        case y7_44:         return @"y7_44";
        case y1_f34:        return @"y1_f34";
        case y2_f34:        return @"y2_f34";
        case y3_f34:        return @"y3_f34";
        case y4_f34:        return @"y4_f34";
        case y5_f34:        return @"y5_f34";
        case y6_f34:        return @"y6_f34";
        case y7_f34:        return @"y7_f34";
        case y1_f44:        return @"y1_f44";
        case y2_f44:        return @"y2_f44";
        case y3_f44:        return @"y3_f44";
        case y4_f44:        return @"y4_f44";
        case y5_f44:        return @"y5_f44";
        case y6_f44:        return @"y6_f44";
        case y7_f44:        return @"y7_f44";
        case y1_age:        return @"y1_age";
        case y2_age:        return @"y2_age";
        case y3_age:        return @"y3_age";
        case y4_age:        return @"y4_age";
        case y5_age:        return @"y5_age";
        case y6_age:        return @"y6_age";
        case y7_age:        return @"y7_age";
        case y1_ageF:       return @"y1_ageF";
        case y2_ageF:       return @"y2_ageF";
        case y3_ageF:       return @"y3_ageF";
        case y4_ageF:       return @"y4_ageF";
        case y5_ageF:       return @"y5_ageF";
        case y6_ageF:       return @"y6_ageF";
        case y7_ageF:       return @"y7_ageF";
        case y1_age34:      return @"y1_age34";
        case y2_age34:      return @"y2_age34";
        case y3_age34:      return @"y3_age34";
        case y4_age34:      return @"y4_age34";
        case y5_age34:      return @"y5_age34";
        case y6_age34:      return @"y6_age34";
        case y7_age34:      return @"y7_age34";
        case y1_age44:      return @"y1_age44";
        case y2_age44:      return @"y2_age44";
        case y3_age44:      return @"y3_age44";
        case y4_age44:      return @"y4_age44";
        case y5_age44:      return @"y5_age44";
        case y6_age44:      return @"y6_age44";
        case y7_age44:      return @"y7_age44";
        case y1_ageF34:     return @"y1_ageF34";
        case y2_ageF34:     return @"y2_ageF34";
        case y3_ageF34:     return @"y3_ageF34";
        case y4_ageF34:     return @"y4_ageF34";
        case y5_ageF34:     return @"y5_ageF34";
        case y6_ageF34:     return @"y6_ageF34";
        case y7_ageF34:     return @"y7_ageF34";
        case y1_ageF44:     return @"y1_ageF44";
        case y2_ageF44:     return @"y2_ageF44";
        case y3_ageF44:     return @"y3_ageF44";
        case y4_ageF44:     return @"y4_ageF44";
        case y5_ageF44:     return @"y5_ageF44";
        case y6_ageF44:     return @"y6_ageF44";
        case y7_ageF44:     return @"y7_ageF44";
        case t_f:           return @"t_f";
        case t_23:          return @"t_23";
        case t_34:          return @"t_34";
        case t_44:          return @"t_44";
        case t_f34:         return @"t_f34";
        case t_f44:         return @"t_f44";
        case t_age:         return @"t_age";
        case t_ageF:        return @"t_ageF";
        case t_age34:       return @"t_age34";
        case t_age44:       return @"t_age44";
        case t_ageF34:      return @"t_ageF34";
        case t_ageF44:      return @"t_ageF44";
        case MeanX1:        return @"MeanX1";
        case MeanX2:        return @"MeanX2";
        case MeanX3:        return @"MeanX3";
        case MeanX4:        return @"MeanX4";
        case MeanX5:        return @"MeanX5";
        case MeanX6:        return @"MeanX6";
        case MeanX7:        return @"MeanX7";
        case MeanY1:        return @"MeanY1";
        case MeanY2:        return @"MeanY2";
        case MeanY3:        return @"MeanY3";
        case MeanY4:        return @"MeanY4";
        case MeanY5:        return @"MeanY5";
        case MeanY6:        return @"MeanY6";
        case MeanY7:        return @"MeanY7";
        case Count1:        return @"Count1";
        case Count2:        return @"Count2";
        case Count3:        return @"Count3";
        case Count4:        return @"Count4";
        case Count5:        return @"Count5";
        case Count6:        return @"Count6";
        case Count7:        return @"Count7";
        default:;
    }
    return @"";
}

- (real)evaluateForCompartment:(int)i atTime:(real)t {
    real b, c, cr, fa;
    --i;
    cr = (1.0 + (prms[Gender] == 0 ?
        (prms[ApoE] == 0 ? prms[t_23]: (prms[ApoE] == 1 ? 0: (prms[ApoE] == 2 ? prms[t_34]: prms[t_44]))):
        (prms[ApoE] == 0 ? prms[t_f] + prms[t_23]: (prms[ApoE] == 1 ? prms[t_f]: (prms[ApoE] == 2 ? prms[t_f] + prms[t_34] + prms[t_f34]: prms[t_f] + prms[t_44] + prms[t_f44])))))
    * (1.0 + (prms[Age] - GeneralTriggerAge - tc[i] + MeanX_) * (prms[Gender] == 0 ?
        (prms[ApoE] <= 1 ? prms[t_age]: (prms[ApoE] == 2 ? prms[t_age] + prms[t_age34]: prms[t_age] + prms[t_age44])):
        (prms[ApoE] <= 1 ? prms[t_age] + prms[t_ageF]: (prms[ApoE] == 2 ? prms[t_age] + prms[t_ageF] + prms[t_age34] + prms[t_ageF34]: prms[t_age] + prms[t_ageF] + prms[t_age44] + prms[t_ageF44]))));
    b = prms[b1 + i * 3] * cr;
    c = prms[c1 + i * 3] * cr;
    fa = (prms[Gender] == 0 ?
        (prms[ApoE] == 0 ? y_23: (prms[ApoE] == 1 ? 0: (prms[ApoE] == 2 ? y_34: y_44))):
        (prms[ApoE] == 0 ? y_f + y_23: (prms[ApoE] == 1 ? y_f: (prms[ApoE] == 2 ? y_f + y_34 + y_f34: y_f + y_44 + y_f44))))
    + (prms[Age] - GeneralMeanAge) * (prms[Gender] == 0 ?
        (prms[ApoE] <= 1 ? y_age: (prms[ApoE] == 2 ? y_age + y_age34: y_age + y_age44)):
        (prms[ApoE] <= 1 ? y_age + y_ageF: (prms[ApoE] == 2 ? y_age + y_ageF + y_age34 + y_ageF34: y_age + y_ageF + y_age44 + y_ageF44)));
    return  i == 4 ?  (prms[c5] == 0 ? prms[a5] + (fa + b / cr) * cr * t : prms[a5] + (b / c - fa) * (exp(c * t) - 1.0)):
    (prms[c1 + i * 3] == 0 ? prms[a1 + i * 3] + fa + b * t : prms[a1 + i * 3] + fa + b / c * (exp(c * t) - 1.0));
}

- (void)preparativeCalculation {
    int i, j;
    real sum = 0, wsum = 0, ssum = 0, ww, ma, mi, err, cr1, cr2;
    cr2 = 1.0 + (prms[Gender] == 0 ?
        (prms[ApoE] == 0 ? prms[t_23]: (prms[ApoE] == 1 ? 0: (prms[ApoE] == 2 ? prms[t_34]: prms[t_44]))):
        (prms[ApoE] == 0 ? prms[t_f] + prms[t_23]: (prms[ApoE] == 1 ? prms[t_f]: (prms[ApoE] == 2 ? prms[t_f] + prms[t_34] + prms[t_f34]: prms[t_f] + prms[t_44] + prms[t_f44]))));
    cr1 = cr2 * (1.0 + (prms[Age] - GeneralTriggerAge) * (prms[Gender] == 0 ?
        (prms[ApoE] <= 1 ? prms[t_age]: (prms[ApoE] == 2 ? prms[t_age] + prms[t_age34]: prms[t_age] + prms[t_age44])):
        (prms[ApoE] <= 1 ? prms[t_age] + prms[t_ageF]: (prms[ApoE] == 2 ? prms[t_age] + prms[t_ageF] + prms[t_age34] + prms[t_ageF34]: prms[t_age] + prms[t_ageF] + prms[t_age44] + prms[t_ageF44]))));
    cr2 *= prms[Gender] == 0 ?
    (prms[ApoE] <= 1 ? prms[t_age]: (prms[ApoE] == 2 ? prms[t_age] + prms[t_age34]: prms[t_age] + prms[t_age44])):
    (prms[ApoE] <= 1 ? prms[t_age] + prms[t_ageF]: (prms[ApoE] == 2 ? prms[t_age] + prms[t_ageF] + prms[t_age34] + prms[t_ageF34]: prms[t_age] + prms[t_ageF] + prms[t_age44] + prms[t_ageF44]));
    for(i = j = 0; i < NumBM; i++, j+= 3) {
        real fa, b = prms[b1 + j], c = prms[c1 + j], m;
        if(MeanY_ == NONE || b == 0.0) continue;
        fa = (prms[Gender] == 0 ?
            (prms[ApoE] == 0 ? y_23: (prms[ApoE] == 1 ? 0: (prms[ApoE] == 2 ? y_34: y_44))):
            (prms[ApoE] == 0 ? y_f + y_23: (prms[ApoE] == 1 ? y_f: (prms[ApoE] == 2 ? y_f + y_34 + y_f34: y_f + y_44 + y_f44))))
        + (prms[Age] - GeneralMeanAge) * (prms[Gender] == 0 ?
            (prms[ApoE] <= 1 ? y_age: (prms[ApoE] == 2 ? y_age + y_age34: y_age + y_age44)):
            (prms[ApoE] <= 1 ? y_age + y_ageF: (prms[ApoE] == 2 ? y_age + y_ageF + y_age34 + y_ageF34: y_age + y_ageF + y_age44 + y_ageF44)));
        if(i == 4)
            m = prms[c5] == 0 ? (MeanY_ - prms[a5]) / (fa + b): log((MeanY_ - prms[a5]) / (b / c - fa) + 1.0) / c;
        else
            m = prms[c1 + j] == 0 ? (MeanY_ - prms[a1 + j] - fa) / b: log((MeanY_ - prms[a1 + j] - fa) * c / b + 1.0) / c;
        tc[i] = cr2 == 0.0 ? m / cr1: (cr1 - sqrt(cr1 * cr1 - 4.0 * cr2 * m)) / cr2 / 2.0;
        ma = prms[plotMax] - MeanX_;
        mi = prms[plotMin] + MeanX_;
        tc[i] = IS_STRANGE(tc[i]) ? (c > 0.0 ? mi: ma): tc[i] < mi ? mi: tc[i] > ma ? ma: tc[i];
        if(prms[enableW] != 0.0) {
            ww = w_;
        } else {
            m = cr1 - cr2 * (tc[i] - MeanX_);
            b *= m;
            c *= m;
            err = [delegate varianceOfIntraErrorForY:MeanY_ ofCompartment:i + 1] / Count_;
            if(c != 0.0) {
                if(i == 4) b -= c * fa;
                ww = err / ((b * exp(c * tc[i])) * (b * exp(c * tc[i])));
            } else {
                if(i == 4) b += fa * m;
                ww = err / (b * b);
            }
            ww = 1 / ww;
        }
        if(IS_STRANGE(ww)) continue;
        m = tc[i] - MeanX_;
        sum += ww * m;
        ssum += ww * m * m;
        wsum += ww;
    }
    if(wsum > 0.0) {
        offsetT = sum / wsum;
        offsetD = sqrt(ssum / wsum - offsetT * offsetT);
    } else {
        offsetT = offsetD = 0.0;
    }
    triggerA = prms[Age] - offsetT;
}

- (BOOL)fixParameterAt:(int)index {
    switch(index) {
        case a1:
        case b1:
        case c1:
        case a2:
        case b2:
        case c2:
        case a3:
        case b3:
        case c3:
        case a4:
        case b4:
        case c4:
        case a5:
        case b5:
        case c5:
        case a6:
        case b6:
        case c6:
        case a7:
        case b7:
        case c7:
            return NO;
        default:;
    }
    return YES;
}

- (real)defaultParameterValueAt:(int)index {
    switch(index) {
        case MeanY1:
        case MeanY2:
        case MeanY3:
        case MeanY4:
        case MeanY5:
        case MeanY6:
        case MeanY7:
            return NONE;
        default:;
    }
    return 0.0;
}

- (BOOL)offsetOfIndependentVariable:(real*)val {
    *val = offsetT;
    return YES;
}

- (int)numberOfExtraValues {
    return 3;
}

- (real)extraValueAt:(int)idx {
    switch (idx) {
        case 0: return offsetT;
        case 1: return offsetD;
    }
    return triggerA;
}

- (NSString*)extraValueNameStringAt:(int)idx {
    switch (idx) {
        case 0: return @"OffsetTime";
        case 1: return @"OffsetDeviation";
    }
    return @"TriggerAge";
}

@end
