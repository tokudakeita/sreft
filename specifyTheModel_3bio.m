
% The biomarker 3 is Abeta concentration in CSF
CSFAbetaN=3;

%% 
GPstr{1}=[0 0 0;0 0 0];GPstr{2}=[0 0 0;0 0 0];
GPstr{3}=[1 0 0;1.5 0 0];%GivenParameter Index and the value for Abeta

GPstrInit{1}=[0 0 0;0 0 0];GPstrInit{2}=[0 0 0;0 0 0];
GPstrInit{3}=[0 0 0;0 0 0];%GivenParameter Index

%%  specifyCovariates

num_cov = 3; % number of the covariates
paramN=3; % number of the paramter of the function
covariates=[];
for j=1:M
    a=realparams.cov_apoe(j);
    g=realparams.cov_gender(j);
    covariates(:,j) = [g, (a==2),(a==3)];
end

A=repmat([[eye(paramN);zeros(1,paramN)],zeros(paramN+1,3)],[1,1,M]);
A(4,4:end,1:M) = covariates;

for i=1:3
    if i ==3
        FEGroupDesignstr{i}=A(2:end,2:end,:);
    else
        FEGroupDesignstr{i}=A;
    end
end
clear A;

for i=1:N
    if i~=3
        normalAstr{i}=[ 1     0     0     0     0     0;  ...
            0     1     0     0     0     0;     0     0  ...
            1     0     0     0;     0     0     0     0     0     0];
    else
        normalAstr{i}=[1     0     0     0     0;  ...
            0     1     0     0     0;     0     0     0     0     0];
    end
end


%% 

% 'REParamsSelect' name-value pair to specify the indices 
% of the parameters to be modeled with random effects in nlmefit 
for i=1:N
    if i==3
        REParamsSelectstr{i}=[1];
        modelstr{i}=@(phi,t)(1.5+(phi(1)/phi(2)-phi(3))*(exp(phi(2)*t)-1));
        dfdphistr{i}=@(phi,t)([phi(2)^(-1)*(exp(phi(2)*t)-1),...
            (t.*(phi(1)/phi(2)-phi(3))...
            -(phi(1)/phi(2))).*(exp(phi(2)*t)-1)-phi(1)/phi(2),...
            -exp(phi(2)*t)+1]);
        dfdtstr{i}=@(phi,t)(phi(2)*(phi(1)/phi(2)-phi(3))*exp(phi(2)*t));
        eta2phistr{i}=@(theta,eta)(theta + ...
            [eta(1);zeros(size(theta,1)-1,1)]);
    else
        REParamsSelectstr{i}=[1 3];
        modelstr{i}=@(phi,t)(phi(1)+phi(4) ...
            +phi(2)/phi(3)*(exp(phi(3)*t)-1));
        dfdtstr{i}=@(phi,t)(phi(2)*exp(phi(3)*t));
        eta2phistr{i}=@(theta,eta)(theta + ...
            [eta(1);0;eta(2);zeros(size(theta,1)-3,1)]);
        dfdphistr{i}=@(phi,t)([1+0*t ,(1/phi(3))*(exp(phi(3)*t)-1),...
            -phi(2)*phi(3)^(-2)*(exp(phi(3)*t)-1) ...
            + phi(2)/phi(3)*t.*exp(phi(3)*t) ,1+0*t]);
    end
end
