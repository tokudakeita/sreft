function [timeEst,subjecttimeEst]= ...
    sreftTimeEstimDoubleExpBayesGoldenSection(modelstr,dfdphistr,ystr, ... 
    eta2phistr,time_sub,subject,N,M,KJ,beta0str,PSIstr,mseM, ... 
    REParamsSelectstr,FEGroupDesignstr,iterationN,tllimit,tulimit)
% Applying Golden Section method
GOLDEN_RATIO = 1.6180339887498948482045868343656;
r=1/(1+GOLDEN_RATIO);
damp=0.5;
GaussNewtonN=10;
sectionN=2;
subjecttimeEst=zeros(1,M);
for j=1:M
    JO=[];
    for section=1:sectionN
        Obj=zeros(1,2);
        left=tllimit+(section-1)*(tulimit-tllimit)/sectionN;
        right=tllimit+(section)*(tulimit-tllimit)/sectionN;
        t0vec=[left,(right-left)*r+left, right-(right-left)*r, right];
        for ind_gs=1:2
            t0=t0vec(ind_gs+1);
            for i=1:N
                nlme_model=modelstr{i};
                dfdphi=dfdphistr{i};
                FEGroupDesign=FEGroupDesignstr{i};
                REParamsSelect=REParamsSelectstr{i};
                A=FEGroupDesign(:,:,j);
                beta=beta0str{i};
                if size(beta,1)<size(beta,2)
                    beta=beta';
                end
                theta=A*beta;
                omega=PSIstr{i};
                sigma=mseM(i);
                y=ystr{i};
                eta2phi=eta2phistr{i};
                f=@(eta,t)(nlme_model(eta2phi(theta,eta),t));
                select=zeros(size(theta,1),size(REParamsSelect,2));
                for ind=1:size(REParamsSelect,2)
                    select(REParamsSelect(ind),ind)=1;
                end
                dfdeta=@(eta,t)(dfdphi(eta2phi(theta,eta),t)*select);
                

                ind=find(subject==j&~isnan(y));
                tsub=time_sub(ind)+t0;
                ysub=y(ind);

                eta0=zeros(size(REParamsSelect,2),1);
                if isempty(ind)
                        eta0=zeros(size(eta0));
                else

                for k=1:GaussNewtonN
                    df=dfdeta(eta0,tsub);
                    eta0 = eta0+damp*omega*df'*(sigma*eye(size(tsub,1))...
                        +df*omega*df')^(-1)*(ysub-f(eta0,tsub)) ... 
                        -damp*(eye(size(omega)) ... 
                        +omega*df'*sigma^(-1)*df)^(-1)*eta0;
                    if ~isempty(find(isnan(eta0)==1, 1))
                        eta0=zeros(size(eta0));
                        break
                    end
                end
                end
                Obj(ind_gs)=Obj(ind_gs)+eta0'*omega^(-1)*eta0 ...
                    +(ysub-f(eta0,tsub))'*sigma^(-1)*(ysub ... 
                    -f(eta0,tsub))+log(det(inv(omega) ... 
                    +df'*df*sigma^(-1)));
            end
        end        
        for it=1:iterationN
            Objtrace(it)=min(Obj);
            haba(it)=right-left;
            if Obj(1)<Obj(2)
                right=t0vec(3);
                Obj(2)=Obj(1);
                Obj(1)=0;
                ind_gs=1;
                t0vec=[left,(right-left)*r+left, ... 
                    right-(right-left)*r, right];
            else
                left=t0vec(2);
                Obj(1)=Obj(2);
                Obj(2)=0;
                ind_gs=2;
                t0vec=[left,(right-left)*r+left, ... 
                    right-(right-left)*r, right];
            end
            t0=t0vec(ind_gs+1);
            for i=1:N
                nlme_model=modelstr{i};
                dfdphi=dfdphistr{i};
                FEGroupDesign=FEGroupDesignstr{i};
                REParamsSelect=REParamsSelectstr{i};
                A=FEGroupDesign(:,:,j);
                beta=beta0str{i};
                if size(beta,1)<size(beta,2)
                    beta=beta';
                end
                theta=A*beta;
                omega=PSIstr{i};
                sigma=mseM(i);
                y=ystr{i};
                eta2phi=eta2phistr{i};
                f=@(eta,t)(nlme_model(eta2phi(theta,eta),t));
                select=zeros(size(theta,1),size(REParamsSelect,2));
                for ind=1:size(REParamsSelect,2)
                    select(REParamsSelect(ind),ind)=1;
                end
                dfdeta=@(eta,t)(dfdphi(eta2phi(theta,eta),t)*select);
                ind=find(subject==j&~isnan(y));
                tsub=time_sub(ind)+t0;
                ysub=y(ind);

                eta0=zeros(size(REParamsSelect,2),1);
                if isempty(ind)
                        eta0=zeros(size(eta0));
                else
                for k=1:GaussNewtonN
                    df=dfdeta(eta0,tsub);
                    eta0 = ...
                        eta0+damp*omega*df'*(sigma*eye(size(tsub,1))...
                        +df*omega*df')^(-1)*(ysub-f(eta0,tsub))...
                        -damp*(eye(size(omega))...
                        +omega*df'*sigma^(-1)*df)^(-1)*eta0;
                    if ~isempty(find(isnan(eta0)==1))
                        eta0=zeros(size(eta0));
                        break
                    end
                end
                end
                Obj(ind_gs)=Obj(ind_gs)+eta0'*omega^(-1)*eta0...
                    +(ysub-f(eta0,tsub))'*sigma^(-1)*(ysub...
                    -f(eta0,tsub))+log(det(inv(omega)+df'*df*sigma^(-1)));
            end
        end
        JO=[JO,[mean(Obj);mean(t0vec(2:3))]];
    end
        subjecttimeEst(j)=JO(2,find(JO(1,:)==min(JO(1,:))));
end
timeEst=subjecttime2time(subjecttimeEst,time_sub,KJ);
end