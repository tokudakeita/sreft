function [O,JO]= calculateObjFOCE(modelstr,dfdphistr,ystr,...
    eta2phistr,subject,time,N,M,beta0str,PSIstr,mseM,...
    REParamsSelectstr,FEGroupDesignstr)


damp=0.5;
GaussNewtonN=15;

    JO=[];
subjecttimeEst=zeros(1,M);
for j=1:M     
            for i=1:N
                nlme_model=modelstr{i};
                dfdphi=dfdphistr{i};
                FEGroupDesign=FEGroupDesignstr{i};
                REParamsSelect=REParamsSelectstr{i};
                A=FEGroupDesign(:,:,j);
                beta=beta0str{i};
                if size(beta,1)<size(beta,2)
                    beta=beta';
                end
                theta=A*beta;
                omega=PSIstr{i};
                sigma=mseM(i);
                y=ystr{i};
                eta2phi=eta2phistr{i};
                f=@(eta,t)(nlme_model(eta2phi(theta,eta),t));
                select=zeros(size(theta,1),size(REParamsSelect,2));
                for ind=1:size(REParamsSelect,2)
                    select(REParamsSelect(ind),ind)=1;
                end
                dfdeta=@(eta,t)(dfdphi(eta2phi(theta,eta),t)*select);
                

                ind=find(subject==j&~isnan(y));
                tsub=time(ind);
                ysub=y(ind);

                eta0=zeros(size(REParamsSelect,2),1);
                for k=1:GaussNewtonN
                    df=dfdeta(eta0,tsub);
                    eta0 = eta0+damp*omega*df'*(sigma*eye(size(tsub,1))...
    +df*omega*df')^(-1)*(ysub-f(eta0,tsub))-damp*(eye(size(omega))...
    +omega*df'*sigma^(-1)*df)^(-1)*eta0;
                    if ~isempty(find(isnan(eta0)==1, 1))
                        eta0=zeros(size(eta0));
                        break
                    end
                end
                JO=[JO,eta0'*omega^(-1)*eta0+(ysub...
    -f(eta0,tsub))'*sigma^(-1)*(ysub-f(eta0,tsub))...
    +log(det(eye(size(omega))+omega*df'*df*sigma^(-1)))...
    +size(tsub,1)*log(det(sigma))];
            end

end

        O=sum(JO);
