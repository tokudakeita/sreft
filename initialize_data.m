

% initialize_data

% This routine generates data

ifdraw=0;% 1 or 0

N=3;%number of the biomarkers
M=400;%number of the subjects
kMin=4; % the minimum number of the observation per subject
kMax=6; % the maximum number of the observation per subject
tmax=20; % maximum subject time
tmin=-5; % minimum subject time
t=[tmin:tmax];
realparams.missinValueP=[0.05 0.01 0.1];
% the probability of missing points for each biomarkers
realparams.xi=[0.2,0.05,0.1];% STD of observation noise
realparams.theta=[    ...
    -2 0.05 0.1; ...
    2 -0.05 0.1;...
    1.5 -0.2 -0.1;]'; % population mean
realparams.omegastr{1}=0.5*[0.25 0 0;0 0 0; 0 0 0.0025];
realparams.omegastr{2}=0.5*[0.36 0 0;0 0 0; 0 0 0.001];
realparams.omegastr{3}=0.1*[0 0 0; 0 0.01 0;0 0 0];
% omega: Variance-Covariance matrix of inter-subject variability


realparams.effect_apoeHetero=0;
realparams.effect_apoeHomo=0;
realparams.effect_gender=-0;

%% 


realparams.covM=realparams.theta(1,:)'* ... 
    [realparams.effect_gender, realparams.effect_apoeHetero,...
    realparams.effect_apoeHomo, ...
    realparams.effect_gender+realparams.effect_apoeHetero, ...
    realparams.effect_gender+realparams.effect_apoeHomo];

Pallel=0.3;%gene frequency
realparams.Papoe=[(1-Pallel)^2, 2*Pallel*(1-Pallel), Pallel^2 ];
% [wild, heterozygous, homozygous]

Papoecum=cumsum(realparams.Papoe);
for j=1:M
    realparams.cov_apoe(j)=1;
    x=rand;
    for ind=1:size(realparams.Papoe,2)-1;
        if Papoecum(ind)<x
            realparams.cov_apoe(j)=ind+1;
        end
    end
end

realparams.Pgender=[0.5 0.5];
Pgendercum=cumsum(realparams.Pgender);
for j=1:M
    realparams.cov_gender(j)=0;
    x=rand;
    for ind=1:size(realparams.Pgender,2)-1;
        if Pgendercum(ind)<x
            realparams.cov_gender(j)=ind;
            
        end
    end
end
clear x;clear Papoecum;clear Pgendercum;clear Pallel

%% 

realparams.time=[];
time_sub=[];
subject=[];
KJ=[];
for j=1:M
    kj=ceil(rand*(kMax-kMin+1))+kMin-1;
    ttemp=rand*(tmax-tmin)+tmin;
    realparams.subjecttime(j)=ttemp;
    realparams.time=[realparams.time;ttemp+[0:kj-1]'];
    time_sub=[time_sub;[0:kj-1]']; % passage time from the first ...
%      observation for each subject
    subject=[subject;j*ones(kj,1)];% subject index for all observation
    KJ=[KJ;kj];% the number of observation for each subject
end
clear ttemp;clear j;clear kj;
clear kMax; clear kMin;clear tmax; clear tmin;

% Now generate the data
realparams.paramtrace=[];
for i=1:N
    omega_m=realparams.omegastr{i};
    sub=0;
    y=[];
    param=[];
    flag=1;
    missP=realparams.missinValueP(i);
    for ind=1:size(realparams.time,1)
       if subject(ind)~=sub;
            flag=1;
            sub=subject(ind);
            param_ij= ...
                MultivariateNormalDist(realparams.theta(:,i),omega_m,1);
            a=realparams.cov_apoe(sub);
            g=realparams.cov_gender(sub);
            if i~=3
            param_ij(1)=param_ij(1)+[g, (a==2),(a==3), ...
                g*(a==2),g*(a==3)]*realparams.covM(i,:)';
            end
            
        end
        ttemp=realparams.time(ind);
        if i~=3
            ytemp=param_ij(1) ...
                +param_ij(2)/param_ij(3)*(exp(param_ij(3)*ttemp) ...
                -1)+realparams.xi(i)*randn;
        else
            ytemp=param_ij(1)+(param_ij(2)/param_ij(3) ...
                -[g, (a==2),(a==3), g*(a==2),g*(a==3) ... 
                ]*realparams.covM(i,:)')*(exp(param_ij(3)*ttemp) ...
                -1)+realparams.xi(i)*randn;
        end
        
        if i>1|flag==0
            if rand<missP
                ytemp=NaN;
            end
        end
        y=[y;ytemp];    
        param=[param,param_ij];
        flag=0;
    end
    ystr{i}=y; % the data of ith biomarker
    realparams.paramtrace=[realparams.paramtrace;param];    
end
clear ttemp;clear ytemp;clear param_ij;
clear omega_m;clear param;clear y;
clear a;

for i=1:N
    y=ystr{i};
    for sub=1:M
        ymean(sub,i)=mean(y(find(subject==sub&~isnan(y))));
    end
end
clear sub;
clear y;
clear i;

if ifdraw==1
    for i=1:N
        subplot(1,N,i)     
        for j=1:M
            ind=find(subject==j);
            y=ystr{i};
            tsub=realparams.time(ind);
            plot(tsub,y(ind));
            hold on;
        end
            hold on;
            legend('off')
    end
    title('Original Data')
    drawnow;
    hid=num2str(clock);hid(find(hid==' '))=[];
    saveas(gcf,[ '00OriginalData.jpg']);
end
realparams.omega=blkdiag(realparams.omegastr{1}, ... 
    realparams.omegastr{2},realparams.omegastr{3});
clear missP
clear i;clear ind;
clear flag;
clear g;
clear t;