## Matlab codes
The sample codes runs a simple Monte-Carlo simulation generating pseudo-clinical data with three biomarkers, and successive execution of SReFT. Extension to apply the method for user’s specific area should be easy. 

*The following Matlab codes are attached.

	Scripts:
	- sreft_main.m
	- initialize_data.m
	- specifyTheModel_3bio.m

	Functions:
	- initialParamEstimation_3bio.m
	- initialTimeEstimationDoubleExp.m
	- sreftTimeEstimDoubleExpBayesGoldenSection.m
	- subjecttime2time.m
	- time2subjecttime.m
	- calculateObjFOCE.m
	- MultivariateNormalDist.m

Save all the files in some folder in the Matlab search path. Running the script “sreft_main” demonstrates the algorithm. All the other scripts and functions are called from the “sreft_main” script.

The program firstly generates pseudo-clinical data with 3 biomarkers like ADNI by calling “initalize_data”. Then, “specifyTheModel_3bio” specifies the model. This includes covariate-model and functions which return Jacobian matrix needed in optimization.



## Napp
The file "DoubleExp_7biomarkers" is the source file of the bundle file of Napp written in Objective-C, which specify the model, and used in the analysis of ADNI  described in the main text. This code can be compiled with Xcode by Apple, and used as an add-in to the coming version of Napp. Then SReFT can be conducted on Napp.


