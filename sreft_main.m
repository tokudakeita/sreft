
% sreft_main

clear all;

fname='3biomarker';

% % % % % % % generate data
ifsave=1;
hid=num2str(clock);hid(find(hid==' '))=[];hid=hid(1:12);
savename=fname;
initialize_data
ifrealparamsGiven=1;
% % % % % % % % % % % % % % % 
ifsaveFig=1;
specifyTheModel_3bio

%% 
tulimit=30;
tllimit=-10;
iterationN=20;
maxT=25;minT=-5;t=[minT:maxT];

%% Set initial value
meanExpextedtime=10;
[phistr_init]=initialParamEstimation_3bio(time_sub,ystr,subject,...
    N,M,GPstrInit,meanExpextedtime);
[timeEstInit]=initialTimeEstimationDoubleExp(time_sub,ymean,...
    subject,KJ,N,M,phistr_init,tulimit,tllimit);

phiCSFAbetaN=phistr_init{CSFAbetaN}; 
phistr_init{CSFAbetaN}=phiCSFAbetaN(2:3);
for i=1:N;beta0str_init{i}=[phistr_init{i},0.0*ones(1,num_cov)];end
%%
figure(1);
for i=1:N
    subplot(1,N,i)
    if license('test','statistics_toolbox')
        gscatter(realparams.time,ystr{i},subject);
        hold on;
        legend('off')
    end
    for j=1:M
        ind=find(subject==j);
        y=ystr{i};
        tsub=realparams.time(ind);
        plot(tsub,y(ind));
    end
end
title('Original Data')
drawnow;
if ifsaveFig==1
    saveas(gcf,[ '00OriginalData.jpg']);
end
%% 
figure(2);
for i=1:N
    subplot(2,ceil(N/2),i)
    if license('test','statistics_toolbox')
        gscatter(timeEstInit,ystr{i},subject);
        legend('off')
        hold on;
    end
    nlme_model=modelstr{i};
    phi=[phistr_init{i} 0];
    plot(t,nlme_model(phi,t));
    title('Initial Estimation before SReFT')
    xlim([-10 30])
end  
if ifsaveFig==1
    saveas(gcf,['0xInitialEstimation.jpg']);
end
%% Begin subject time estimation 


options.FunValCheck='off';

phi0=phistr_init;
beta0str=beta0str_init;
time0=timeEstInit;
subjecttimeEst=time2subjecttime(time0,time_sub,KJ);
aictrace=[];
covtrace=[];
PSInormMatEsttrace=[];
phitrace=[];
ObjAfTimeEstim=[];

phiMatEsttrace=[];
PSIMatEsttrace=[];
mseMtrace=[];
% set(gcf,'Position',[ 500 100 500 800]);

%% 

for iteration= 1:10
    iteration
    phiMatEst=[];
    cov=[];
    PSIMatEst=[];
    omegaEst=[];
        %%   estimating the hyperparamter
for i=1:N
    i
    y=ystr{i};
    nonnan=find(~isnan(y)&~isnan(time0));
    excludedSubj=find(ismember([1:M],subject(nonnan))==0);
    nlme_model=modelstr{i};
    REParamsSelect=REParamsSelectstr{i};
    FEGroupDesign=FEGroupDesignstr{i};
    FEGroupDesign(:,:,excludedSubj)=[];
    GP=GPstr{i};
    beta0=beta0str{i};
    tic;
    [beta,PSI,stats]=nlmefit(time0(nonnan),y(nonnan),subject(nonnan),...
        [],nlme_model,beta0,'REParamsSelect',REParamsSelect,...
        'FEGroupDesign',FEGroupDesign,'RefineBeta0','off');
%     [beta,PSI,stats]=nlmefit(time0(nonnan),y(nonnan),...
% subject(nonnan),[],nlme_model,beta0,'REParamsSelect',...
% REParamsSelect,'FEGroupDesign',FEGroupDesign,'RefineBeta0',...
% 'off','ApproximationType','FOCE');
    phi=beta;
    
       
    toc
    
    if isequal(GP(1,:),[0 0 0])
        phiful=beta(1:3);
        if isequal(REParamsSelect,[1 3])
            PSIful=[PSI(1,1),0,PSI(1,2);0 0 0;PSI(1,2),0 PSI(2,2)];
        else
            error('error');
        end
    elseif isequal(GP(1,:),[1 0 0])
        phiful=[GP(2,1);beta(1:2)];
        if isequal(REParamsSelect,1)
            PSIful=blkdiag(0,PSI,0);
        else
            error('error');
        end
    else
        error('error');
    end
        
        
    phiMatEst=[phiMatEst,phiful'];
    PSIMatEst=[PSIMatEst,PSIful(1:end)];
    beta0str{i}=beta;
    
    phistrEst{i}=phiful;
    PSIstrEst{i}=PSI;
    mseM(i)=stats.mse;
    cov=[cov,diag(PSIful)'];
    omegaEst=blkdiag(omegaEst,PSIful);
end
phistrEst{:}
PSIstrEst{:}
mseM

phiall=[phistrEst{1}',phistrEst{2}',phistrEst{3}'];
phitrace=[phitrace;phiall];
covtrace=[covtrace;cov];
aictrace=[aictrace,stats.aic];
phiMatEsttrace=[phiMatEsttrace;phiMatEst];
PSIMatEsttrace=[PSIMatEsttrace;PSIMatEst];
if ifrealparamsGiven==1;
    PSInormMatEsttrace=...
        [PSInormMatEsttrace,omegaEst(find(realparams.omega~=0))./...
        realparams.omega(find(realparams.omega~=0))];
end

mseMtrace=[mseMtrace;mseM];

%% Estimating the "subject time"

subjecttimeEst_bf=subjecttimeEst;
 [timeEst,subjecttimeEst]= ...
     sreftTimeEstimDoubleExpBayesGoldenSection(modelstr,dfdphistr,...
     ystr,eta2phistr,time_sub,subject,N,M,KJ,beta0str,PSIstrEst,...
     mseM,REParamsSelectstr,FEGroupDesignstr,10,tllimit,tulimit);

[O,JO]= calculateObjFOCE(modelstr,dfdphistr,ystr,eta2phistr,...
     subject,timeEst,N,M,beta0str,PSIstrEst,mseM,...
     REParamsSelectstr,FEGroupDesignstr);
ObjAfTimeEstim=[ObjAfTimeEstim,O];

%% 

figure(3);
hold off
for i=1:N
    subplot(1,N+1,i)
    hold off;
%     gscatter(timeEst,ystr{i},subject);
    plot(timeEst,ystr{i},'.');
    legend('off')
    hold on;
    nlme_model=modelstr{i};
    FEGroupDesign=FEGroupDesignstr{i};
        A=normalAstr{i};
    beta=beta0str{i};
    plot(t,nlme_model(beta,t),'k','linewidth',2);
    if ifrealparamsGiven==1
        if i==CSFAbetaN
            beta(1:2)=realparams.theta(2:3,i);
        else
            beta(1:3)=realparams.theta(1:3,i);
        end
        plot(t,nlme_model(A*beta,t),'r');
    end
%     title('Initial Estimation before SReFT')
end
if ifrealparamsGiven==1
    subplot(1,N+1,N+1)
    plot(realparams.subjecttime,subjecttimeEst,'.');
end
set(gcf,'Position', [100 200 1200 200]);
if ifsaveFig==1
    saveas(gcf,[num2str(iteration),'_', savename '3.jpg']);
end
%% 

figure(4);
subplot(4,1,1)
hold off;
thetaind=[];
for i= 1:N
    thetaind=[thetaind,(i-1)*6+1:(i-1)*6+3];
end
if ifrealparamsGiven==1
plot(phiMatEsttrace./(ones(iteration,1)*(realparams.theta(1:end))))
hold on;plot([-1:iteration],[-1:iteration]*0+1,'k')
xlim([-1,iteration])
ylim([0 2])
end

subplot(4,1,2)
hold off;
if ifrealparamsGiven==1
    plot([1:iteration],PSInormMatEsttrace);hold on;
    plot([-1:iteration],[-1:iteration]*0+1,'k')
    xlim([-1 iteration]);
    ylim([0 3])
end

subplot(4,1,3)
hold off;
if ifrealparamsGiven==1
    plot([1:iteration],mseMtrace.^(1/2)./(ones(iteration,1)...
     *realparams.xi));
    hold on;
    plot([-1:iteration],[-1:iteration]*0+1,'k')
end
xlim([-1 iteration]);
ylim([0 2])

subplot(4,1,4)
plot(aictrace);
xlim([-1 iteration]);
drawnow;
if ifsaveFig==1
    drawnow;pause(0.1);
    saveas(gcf,[savename  '4.jpg']);
    saveas(gcf,[savename   '4.fig'])
end


%% 

time0=timeEst;
if ifsave==1
save([savename  '.mat'])
end
end

