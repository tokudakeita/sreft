function [timeEstInit]=...
    initialTimeEstimationDoubleExp(time, ...
    ymean,subject,KJ,N,M,phistr_init,tulimit,tllimit)

tEst=nan(M,N);
for i=1:N
    phi=phistr_init{i};
%     tEst(:,i)=(phi(3)>0)*tllimit+(phi(3)<=0)*tulimit;
    ym=ymean(:,i);
    term=(phi(3)/phi(2)*(ym-phi(1)))+1;
    ind=find(term>0);
    tEst(ind,i)=log(term(ind))/phi(3);
    
      
end

timeEstInit=[];
for j=1:M
    ind=find(~isnan(tEst(j,:)));
    subjectime=mean(tEst(j,ind));
    ind=find(subject==j);
%     if isempty(ind)
%         t=0;
%     else
    t=time(ind);
% end
    
    timeEstInit=[timeEstInit;	t-mean(t)+subjectime];
    
    timeEstInit(find(timeEstInit>tulimit |timeEstInit<tllimit))=NaN;
end