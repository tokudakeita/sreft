function [time]=subjecttime2time(subjecttime,time_sub,KJ)

if size(KJ,1)<size(KJ,2)
    KJ=KJ';
end

if size(subjecttime,1)<size(subjecttime,2)
    subjecttime=subjecttime';
end

if size(time_sub,1)<size(time_sub,2)
    time_sub=time_sub';
end

if (size(subjecttime)-size(KJ))*(size(subjecttime)-size(KJ))' ~=0
    error('The size of the vectors must be the same.')
end

if size(time_sub,1)~=sum(KJ)
    error('error')
end


time=[];

for j=1:size(KJ,1)
    if j>1
        ind=[sum(KJ(1:j-1))+1:sum(KJ(1:j))];
    else
        ind=1:KJ(1);
    end
    
    time=[time;subjecttime(j)+time_sub(ind)];
end
    