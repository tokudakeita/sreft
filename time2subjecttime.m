function [subjecttime]=time2subjecttime(time,time_sub,KJ)

if size(KJ,1)<size(KJ,2)
    KJ=KJ';
end

if size(time_sub,1)<size(time_sub,2)
    time_sub=time_sub';
end

if size(time_sub,1)~=sum(KJ)
    error('error')
end

subjecttime=[];

for i=1:size(KJ,1)
    
    if i==1
        ind=1;
    else
        ind=sum(KJ(1:i-1))+1;
    end
    subjecttime=[subjecttime,time(ind)];
end