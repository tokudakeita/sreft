function [x] =MultivariateNormalDist(mu,sigma,n)

if size(mu,1)<size(mu,2)
    mu=mu';
end

if size(mu,2)~=1;
    mu
    error('mean value must be a vector')
end

if size(sigma,1)~=size(sigma,2)
    sigma
    error('covariance matrix must be a square matrix')
end

if ~isempty(find((sigma==sigma')==0));
    sigma
    error('covariance matrix must be symmetric')
end

if size(mu,1)~=size(sigma,1)
    mu
    sigma
    error(['no match between dimensions of mean vector and ' ...
    'covariance matrix']);
end
dim=size(mu,1);

x=sigma^(1/2)*randn(dim,n)+mu*ones(1,n); 