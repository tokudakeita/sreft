function [phistr_init]=initialParamEstimation_3bio(time, ...
    ystr,subject,N,M,GPstrInit,meanExpextedtime)

phi=zeros(1,3);
for i=1:N
    y=ystr{i};
    meanY=[];
    slope=[];
    for j=1:M
        ind=find(subject==j&~isnan(y));
        if length(ind)>2
            ysb=y(ind);
            [a b]=polyfit(time(ind),ysb,1);
            slope=[slope;a(1)];
            meanY=[meanY;mean(ysb)];
        end
    end
    GP=GPstrInit{i};

    if license('test','statistics_toolbox')    
        if isequal(GP(1,:),[0 0 0])
            a=polyfit(meanY,slope,1);
            phi(3)=a(1);
            phi(2)=mean(slope)/exp(a(1)*meanExpextedtime);
            phi(1)=(phi(2)-a(2))/a(1);

        elseif isequal(GP(1,:),[1 0 0])
            a=polyfit(meanY-GP(2,1),slope,1);
            phi(3)=a(1);
            phi(2)=a(2);
            phi(1)=GP(2,1);
        elseif isequal(GP(1,:),[0 1 0])
            a=polyfit(meanY,slope,1);
            phi(3)=a(1);
            phi(2)=GP(2,2);
            phi(1)=(phi(2)-a(2))/a(1);
        elseif isequal(GP(1,:),[0 0 1])
            model=@(a,Y)(GP(2,3)*(Y-a(1))+a(2));
            a=nlinfit( meanY,slope,model,[0]);
            phi(3)=GP(2,3);
            phi(1)=a(1);
            phi(2)=a(2);
        else
            error('error')
        end
    else
        if isequal(GP(1,:),[1 0 0])
            a=polyfit(meanY-GP(2,1),slope,1);
            phi(3)=a(1);
            phi(2)=a(2);
            phi(1)=GP(2,1);
        else
            a=polyfit(meanY,slope,1);
            phi(3)=a(1);
            phi(2)=mean(slope)/exp(a(1)*meanExpextedtime);
            phi(1)=(phi(2)-a(2))/a(1);
            ind=find(GP(1,:)==1);
            phi(ind)=GP(2,ind);
        end
    end    
    phistr_init{i}=phi;
end